.PHONY: build

build:
	docker build -f Containerfile -t registry.gitlab.com/bootc-org/tests/testing-framework/provisioning-bootc .
